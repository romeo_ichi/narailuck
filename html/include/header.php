<header>
    <div id="header">
        <div id="header_inner" class="container">
            <div class="row">
                
                <div class="col-md-6 col-xs-9">
                    <div id="header_logo"><a href="#"><img src="assets/images/logo.png" class="img-responsive" /></a></div>
                </div>
                <div class="col-xs-3 visible-xs visible-sm">
                    <button class="sb-toggle-right btn btn-lg btn-primary">
                        <i aria-hidden="true" class="fa fa-navicon"></i>
                    </button>
                </div>
                <div class="col-md-6 hidden-xs hidden-sm">
                    <div class="topbar pull-right">
                        <div class="search">
                            <form>
                                <div class="input-group">
                                  <input type="text" class="form-control" placeholder="Search for...">
                                  <span class="input-group-btn">
                                    <button class="btn btn-primary" type="button"><i aria-hidden="true" class="fa fa-search"></i> ค้นหา</button>
                                  </span>
                                </div><!-- /input-group -->
                            </form>
                        </div>
                        
                        <div id="header_lang">
                            <a href="#"><img src="assets/images/th.png" /></a>  
                            <a href="#"><img src="assets/images/en.png" /></a>
                            <a href="#"><img src="assets/images/ch.png" /></a>
                        </div>
                        <div id="header_facebook"><a href="https://www.facebook.com/profile.php?id=100012324033300&fref=ts&ref=br_tf" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
    <div id="nav" class="hidden-xs hidden-sm">
        <div class="container">
        	<?php include('include/menu.php') ?>
            
        </div>
        
    </div>
</header>