<!-- Bootstrap -->
    <link href="assets/plugins/bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">
    <!--FontAwesome-->
    <link href="assets/plugins/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <!-- Slidebars CSS -->
    <link rel="stylesheet" href="assets/plugins/Slidebars-0.10.3/dist/slidebars.min.css">
	<!-- Owl Carousel Assets -->
    <link href="assets/plugins/owl.carousel/owl-carousel/owl.carousel.css" rel="stylesheet">
    <link href="assets/plugins/owl.carousel/owl-carousel/owl.theme.css" rel="stylesheet">
    
    <!--Style Default-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css" />