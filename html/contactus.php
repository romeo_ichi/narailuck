<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php include('include/title.php') ?>
	
    <?php include('include/style.php') ?>
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>

<div id="sb-site">
	<?php include('include/header.php') ?>
    
	<div class="main">
    	<div class="container">
        	<?php include('include/slider.php') ?>
        	
            <?php include('include/welcome.php') ?>
            
            <section class="home-category contactus">
            	<div class="cat_sec">
                    <h2>ติดต่อเรา</h2>
                    <div class="pro_sec">
                    	<div class="row">
                            <div class="col-md-8">
                                <div class="well well-sm">
                                    <form>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">
                                                    Name</label>
                                                <input type="text" class="form-control" id="name" placeholder="Enter name" required="required" />
                                            </div>
                                            <div class="form-group">
                                                <label for="email">
                                                    Email Address</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                                    </span>
                                                    <input type="email" class="form-control" id="email" placeholder="Enter email" required="required" /></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="subject">
                                                    Subject</label>
                                                <select id="subject" name="subject" class="form-control" required="required">
                                                    <option value="na" selected="">Choose One:</option>
                                                    <option value="service">General Customer Service</option>
                                                    <option value="suggestions">Suggestions</option>
                                                    <option value="product">Product Support</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="name">
                                                    Message</label>
                                                <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                                    placeholder="Message"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                                Send Message</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <form>
                                <legend><span class="glyphicon glyphicon-globe"></span> นารายณ์รักษ์</legend>
                                <address>
                                                                    ดำเนินงานโดย <br />ธนภูมิ(ศานิต) สุวัณณะศรี <br /> ศูนย์พระเครื่องมรดกไทย ห้างพันธุ์ทิพย์ งามวงศ์วาน ชั้น 7
        <br />
                                  		<i class="fa fa-phone" aria-hidden="true"></i>โทร <a href="tel:+66972407664">097-240-7664</a> | <i class="fa fa-envelope" aria-hidden="true"></i> อีเมล <a href="mailto:n-arai@hotmail.com">n-arai@hotmail.com</a>

                                </address>
                                </form>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </section>

            
        </div>
    </div>

	
	<?php include('include/footer.php') ?>
  
</div>

<?php include('include/navmobile.php') ?>




<?php include('include/script.php') ?>




</body>
</html>
