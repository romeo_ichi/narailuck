<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php include('include/title.php') ?>
	
    <?php include('include/style.php') ?>
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>

<div id="sb-site">
	<?php include('include/header.php') ?>
    
	<div class="main">
    	<div class="container">
        	<?php include('include/slider.php') ?>
        	
            <?php include('include/welcome.php') ?>
            
            <section class="home-category">
            	<div class="cat_sec">
            	<h2>สาระน่ารู้</h2>
                <div class="pro_sec">
                	<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                    	<table class="table table-hover" align="center" cellpadding="0" border="0" cellspacing="0" >
                            <tr>
                                <td>
                                    <a href="knowledgedetail.php"><img src="/assets/images/propic.jpg"/></a>
                                </td>
                                <td>
                                    <a href="knowledgedetail.php"><b>พระใหม่มาแรงปี๕๖เหรียญหลวงพ่อคูณรุ่นเมตตา</b><br />
    พระใหม่มาแรงแห่งปี๒๕๕๖เหรียญหลวงพ่อคูณ ปริสุทโธ รุ่นเมตตา : เรื่องและภาพ ไตรเทพ ไกรงู</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="knowledgedetail.php"><img src="/assets/images/propic.jpg"/></a>
                                </td>
                                <td>
                                    <a href="knowledgedetail.php"><b>พระใหม่มาแรงปี๕๖เหรียญหลวงพ่อคูณรุ่นเมตตา</b><br />
    พระใหม่มาแรงแห่งปี๒๕๕๖เหรียญหลวงพ่อคูณ ปริสุทโธ รุ่นเมตตา : เรื่องและภาพ ไตรเทพ ไกรงู</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                	
                </div>
                <div class="propage">
                    <a href="#"><</a> <a href="#">Previous</a> | <a href="#">1</a> | <a href="#">2</a> | <a href="#">3</a> | <a href="#">4</a> | <a href="#">5</a> | <a href="#">Next</a> <a href="#">></a>
                </div>
            </div>
            </section>

            
        </div>
    </div>

	
	<?php include('include/footer.php') ?>
  
</div>

<?php include('include/navmobile.php') ?>




<?php include('include/script.php') ?>




</body>
</html>
