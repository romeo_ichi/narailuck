<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php include('include/title.php') ?>
	
    <?php include('include/style.php') ?>
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>

<div id="sb-site">
	<?php include('include/header.php') ?>
    
	<div class="main">
    	<div class="container">
        	<?php include('include/slider.php') ?>
        	
            <?php include('include/welcome.php') ?>
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2>ประเภทพระ</h2>
                    <div class="pro_sec">
                        <ul class="cate_list">
                            <li><a href="#">พระบูชา</a></li>
                            <li><a href="#">พระเบญจภาคี</a></li>
                            <li><a href="#">เครื่องราง</a></li>
                            <li><a href="#">พระปิดตา</a></li>
                            <li><a href="#">พระกรุ</a></li>
                            <li><a href="#">พระเนื้อว่าน</a></li>
                            <li><a href="#">พระกริ่ง</a></li>
                            <li><a href="#">พระชัย</a></li>
                            <li><a href="#">พระเนื้อชิน</a></li>
                            <li><a href="#">พระงาแกะ</a></li>
                            <li><a href="#">พระไม้แกะ</a></li>
                            <li><a href="#">พระรูปหล่อ</a></li>
                            <li><a href="#">พระเหรียญ</a></li>
                            <li><a href="#">พระเหรียญปั้ม</a></li>
                            <li><a href="#">พระเนื้อผง</a></li>
                            <li><a href="#">พระเนื้อดิน</a></li>
                            <li><a href="#">ผ้ายันต์</a></li>
                            <li><a href="#">ภาพเก่าเกจิอาจารย์ดัง</a></li>
                            <li><a href="#">ผงคลุกรัก</a></li>
                            <li><a href="#">เฟอร์นิเจอร์เก่า</a></li>
                            <li><a href="#">หนังสือพระเครื่องเก่า</a></li>
                        </ul>
                    </div>
                </div>
            </section>

            
        </div>
    </div>

	
	<?php include('include/footer.php') ?>
  
</div>

<?php include('include/navmobile.php') ?>




<?php include('include/script.php') ?>




</body>
</html>
