(function($) {
	$(document).ready(function() {
	  $.slidebars();
	  $("#owl-example").owlCarousel({
	  	singleItem:true,
		autoPlay:true
	  });
	  $("#owl-homecategory1").owlCarousel({
		autoPlay:true,
		items : 4,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [980,3],
		itemsTablet: [768,2],
		itemsTabletSmall: false,
		itemsMobile : [479,1],
	  });
	  $("#owl-homecategory2").owlCarousel({
		autoPlay:true,
		items : 4,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [980,3],
		itemsTablet: [768,2],
		itemsTabletSmall: false,
		itemsMobile : [479,1],
	  });
	  $("#owl-homecategory3").owlCarousel({
		autoPlay:true,
		items : 4,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [980,3],
		itemsTablet: [768,2],
		itemsTabletSmall: false,
		itemsMobile : [479,1],
	  });
	  $("#owl-homecategory4").owlCarousel({
		autoPlay:true,
		items : 4,
		itemsDesktop : [1199,4],
		itemsDesktopSmall : [980,3],
		itemsTablet: [768,2],
		itemsTabletSmall: false,
		itemsMobile : [479,1],
	  });
	});
  }) (jQuery);
  