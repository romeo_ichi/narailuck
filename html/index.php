<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php include('include/title.php') ?>
	
    <?php include('include/style.php') ?>
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>

<div id="sb-site">
	<?php include('include/header.php') ?>
    
	<div class="main">
    	<div class="container">
        	<?php include('include/slider.php') ?>
            
            <?php include('include/welcome.php') ?>
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2><a href="productlist.php">พระใหม่ ของขลัง เครื่องราง ผ้ายันต์ บาตรน้ำมนต์ ภาพเก่า</a></h2>
                    <div class="pro_sec">
                        <div id="owl-homecategory1" class="owl-carousel">
							<?php for($i=0;$i<8;$i++)  { ?>
                                <div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="productdetail.php" target="_blank"><img src="assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="productdetail.php" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="productdetail.php" target="_blank"><img src="assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="productdetail.php" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>
            
            <section class="banner728">
            	<a href="#" target="_blank"><img src="assets/images/banner728-1.gif" class="img-responsive" /></a>
            </section>
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2><a href="productlist.php">ของเก่า ตู้รัตนะไม้เก่า ตู้ไม้สักเก่า ถ้วยชามสมัยต่างๆ เบญจรงค์ สังคโลก</a></h2>
                    <div class="pro_sec">
                        <div id="owl-homecategory2" class="owl-carousel">
							<?php for($i=0;$i<8;$i++)  { ?>
                                <div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="productdetail.php" target="_blank"><img src="assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="productdetail.php" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="productdetail.php" target="_blank"><img src="assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="productdetail.php" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>
            
            <section class="banner728">
            	<a href="#" target="_blank"><img src="assets/images/banner728-2.gif" class="img-responsive" /></a>
            </section>
            
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2><a href="productlist.php">โฉนดที่ดิน</a></h2>
                    <div class="pro_sec">
                        <div id="owl-homecategory3" class="owl-carousel">
							<?php for($i=0;$i<8;$i++)  { ?>
                                <div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="productdetail.php" target="_blank"><img src="assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="productdetail.php" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="productdetail.php" target="_blank"><img src="assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="productdetail.php" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>
            
            <section class="banner728">
            	<a href="#" target="_blank"><img src="assets/images/banner728x90.jpg" class="img-responsive" /></a>
            </section>
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2><a href="productlist.php">ที่ดินน.ส.3ก บท.5</a></h2>
                    <div class="pro_sec">
                        <div id="owl-homecategory4" class="owl-carousel">
							<?php for($i=0;$i<8;$i++)  { ?>
                                <div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="productdetail.php" target="_blank"><img src="assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="productdetail.php" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="productdetail.php" target="_blank"><img src="assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="productdetail.php" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>
            
            
        </div>
    </div>

	
	<?php include('include/footer.php') ?>
  
</div>

<?php include('include/navmobile.php') ?>




<?php include('include/script.php') ?>




</body>
</html>
