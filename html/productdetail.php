<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php include('include/title.php') ?>
	
    <?php include('include/style.php') ?>
    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>


<body>

<div id="sb-site">
	<?php include('include/header.php') ?>
    
	<div class="main">
    	<div class="container">
        	<?php include('include/slider.php') ?>
        	
            <?php include('include/welcome.php') ?>
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2>พระบูชา ไม้แก่นจันทร์ หลวงปู่บุญ วัดกลางบางแก้ว </h2>
                    <div class="pro_sec">
                    	<div class="col-md-8">
                            <div class="proD_img">
                            	<div class="row">
                                    <div class="col-sm-6">
                                        <img src="/assets/images/detail_fpic.jpg" class="img-responsive" />
                                        <figcaption>ด้านหน้า</figcaption>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="/assets/images/detail_fpic.jpg" class="img-responsive" />
                                        <figcaption>ด้านหลัง</figcaption>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="/assets/images/detail_fpic.jpg" class="img-responsive" />
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="/assets/images/detail_fpic.jpg" class="img-responsive" />
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="/assets/images/detail_fpic.jpg" class="img-responsive" />
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                        	<div class="proD_contents">
                            	<dl>
                                  <dt>รุ่นพระ</dt>
                                  <dd>พระบูชา ไม้แก่นจันทร์ หลวงปู่บุญ วัดกลางบางแก้ว</dd>
                                  <dt>ประเภทพระ</dt>
                                  <dd>พระบูชา</dd>
                                  <dt>รายละเอียด</dt>
                                  <dd>
                                 สิงห์หลวงพ่อหอมตัวใหญ่สวยเดิมน่าสะสมบูชาตัวใหญ่สวยเดิม
                                  </dd>
                                  <dt>ราคา</dt>
                                  <dd>
                                  		โทรสอบถาม
                                  </dd>
                                  <dt>ติดต่อ</dt>
                                  <dd>
                                  		ร้าน นารายณ์รักษ์ พันธ์ทิพย์ ชั้น 7 งามวงศ์วาน  เปิดร้านตั้งแต่ เวลา 10.30น - 20.00น<br />
                                  		<i aria-hidden="true" class="fa fa-phone"></i>โทร <a href="tel:+66972407664">097-240-7664</a> | <i aria-hidden="true" class="fa fa-envelope"></i> อีเมล <a href="mailto:n-arai@hotmail.com">n-arai@hotmail.com</a>
                                  </dd>
                                  <dt>ช่องทางการชำระเงิน</dt>
                                  <dd>
                                  		ธนาคารไทยพาณิชย์ <br />สาขาพันทิพย์ พลาซ่า งามวงศ์วาน<br />
                                  		เลขที่บัญชี <strong>388-211639-2</strong><br />
ชื่อบัญชี <strong>นายเกษม สุวัณณะศรี</strong>
                                  </dd>
                                </dl>
                                
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </section>

            
        </div>
    </div>

	
	<?php include('include/footer.php') ?>
  
</div>

<?php include('include/navmobile.php') ?>




<?php include('include/script.php') ?>



</body>
</html>
