<?php include('../theme/include/header.php') ?>

	<div class="main">
        <div class="container">
            <?php include('../theme/include/slider.php') ?>
            
            <?php include('../theme/include/welcome.php') ?>
            
            <section class="home-category">
                <div class="cat_sec">
                <h2>สาระน่ารู้</h2>
                <div class="pro_sec">
                    <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                        <table class="table table-hover" align="center" cellpadding="0" border="0" cellspacing="0" >
                            <tr>
                                <td>
                                    <a href="detail.php"><img src="../theme/assets/images/propic.jpg"/></a>
                                </td>
                                <td>
                                    <a href="detail.php"><b>พระใหม่มาแรงปี๕๖เหรียญหลวงพ่อคูณรุ่นเมตตา</b><br />
    พระใหม่มาแรงแห่งปี๒๕๕๖เหรียญหลวงพ่อคูณ ปริสุทโธ รุ่นเมตตา : เรื่องและภาพ ไตรเทพ ไกรงู</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="detail.php"><img src="../theme/assets/images/propic.jpg"/></a>
                                </td>
                                <td>
                                    <a href="detail.php"><b>พระใหม่มาแรงปี๕๖เหรียญหลวงพ่อคูณรุ่นเมตตา</b><br />
    พระใหม่มาแรงแห่งปี๒๕๕๖เหรียญหลวงพ่อคูณ ปริสุทโธ รุ่นเมตตา : เรื่องและภาพ ไตรเทพ ไกรงู</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                </div>
                <div class="propage">
                    <a href="#"><</a> <a href="#">Previous</a> | <a href="#">1</a> | <a href="#">2</a> | <a href="#">3</a> | <a href="#">4</a> | <a href="#">5</a> | <a href="#">Next</a> <a href="#">></a>
                </div>
            </div>
            </section>

            
        </div>
    </div>

<?php include('../theme/include/footer.php') ?>
	