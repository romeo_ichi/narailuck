<?php include('../theme/include/header.php') ?>

	<div class="main">
        <div class="container">
            <?php include('../theme/include/slider.php') ?>
            
            <?php include('../theme/include/welcome.php') ?>
            
            <section class="home-category">
                <div class="cat_sec">
                    <h2>ประเภทพระ</h2>
                    <div class="pro_sec">
                        <ul class="cate_list">
                            <li><a href="#">พระบูชา</a></li>
                            <li><a href="#">พระเบญจภาคี</a></li>
                            <li><a href="#">เครื่องราง</a></li>
                            <li><a href="#">พระปิดตา</a></li>
                            <li><a href="#">พระกรุ</a></li>
                            <li><a href="#">พระเนื้อว่าน</a></li>
                            <li><a href="#">พระกริ่ง</a></li>
                            <li><a href="#">พระชัย</a></li>
                            <li><a href="#">พระเนื้อชิน</a></li>
                            <li><a href="#">พระงาแกะ</a></li>
                            <li><a href="#">พระไม้แกะ</a></li>
                            <li><a href="#">พระรูปหล่อ</a></li>
                            <li><a href="#">พระเหรียญ</a></li>
                            <li><a href="#">พระเหรียญปั้ม</a></li>
                            <li><a href="#">พระเนื้อผง</a></li>
                            <li><a href="#">พระเนื้อดิน</a></li>
                            <li><a href="#">ผ้ายันต์</a></li>
                            <li><a href="#">ภาพเก่าเกจิอาจารย์ดัง</a></li>
                            <li><a href="#">ผงคลุกรัก</a></li>
                            <li><a href="#">เฟอร์นิเจอร์เก่า</a></li>
                            <li><a href="#">หนังสือพระเครื่องเก่า</a></li>
                        </ul>
                    </div>
                </div>
            </section>

            
        </div>
    </div>

<?php include('../theme/include/footer.php') ?>
	