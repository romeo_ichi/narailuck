<?php include('../theme/include/header.php') ?>

	<div class="main">
    	<div class="container">
        	<?php include('../theme/include/slider.php') ?>
            
            <?php include('../theme/include/welcome.php') ?>
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2><a href="productlist.php">พระใหม่ ของขลัง เครื่องราง ผ้ายันต์ บาตรน้ำมนต์ ภาพเก่า</a></h2>
                    <div class="pro_sec">
                        <div id="owl-homecategory1" class="owl-carousel">
							<?php for($i=0;$i<8;$i++)  { ?>
                                <div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="../amulets/detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="../amulets/detail.php" target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="../amulets/detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="../amulets/detail.php" target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>
            
            <section class="banner728">
            	<a href="#" target="_blank"><img src="../theme/assets/images/banner728-1.gif" class="img-responsive" /></a>
            </section>
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2><a href="productlist.php">ของเก่า ตู้รัตนะไม้เก่า ตู้ไม้สักเก่า ถ้วยชามสมัยต่างๆ เบญจรงค์ สังคโลก</a></h2>
                    <div class="pro_sec">
                        <div id="owl-homecategory2" class="owl-carousel">
							<?php for($i=0;$i<8;$i++)  { ?>
                                <div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="../antiques/detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="../antiques/detail.php" target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="../antiques/detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="../antiques/detail.php" target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>
            
            <section class="banner728">
            	<a href="#" target="_blank"><img src="../theme/assets/images/banner728-2.gif" class="img-responsive" /></a>
            </section>
            
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2><a href="productlist.php">โฉนดที่ดิน</a></h2>
                    <div class="pro_sec">
                        <div id="owl-homecategory3" class="owl-carousel">
							<?php for($i=0;$i<8;$i++)  { ?>
                                <div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="../land/detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="../land/detail.php" target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="../land/detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="../land/detail.php" target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>
            
            <section class="banner728">
            	<a href="#" target="_blank"><img src="../theme/assets/images/banner728x90.jpg" class="img-responsive" /></a>
            </section>
            
            <section class="home-category">
            	<div class="cat_sec">
                    <h2><a href="productlist.php">ที่ดินน.ส.3ก บท.5</a></h2>
                    <div class="pro_sec">
                        <div id="owl-homecategory4" class="owl-carousel">
							<?php for($i=0;$i<8;$i++)  { ?>
                                <div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="../land/detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="../land/detail.php" target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="../land/detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="../land/detail.php" target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </section>
            
            
        </div>
    </div>

<?php include('../theme/include/footer.php') ?>
	