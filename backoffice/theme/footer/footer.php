 </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../theme/assets/js/jquery.js"></script>
    <script src="../theme/assets/js/jquery-1.8.3.min.js"></script>
    <script src="../theme/assets/js/bootstrap.min.js"></script>
	<script src="../theme/assets/plugins/jquery.validate.th.js"></script>
    <script src="../theme/assets/plugins/jquery.form.min.js"></script>
    <script class="include" type="text/javascript" src="../theme/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../theme/assets/js/jquery.scrollTo.min.js"></script>
    <script src="../theme/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../theme/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="../theme/assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="../theme/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="../theme/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="../theme/assets/js/sparkline-chart.js"></script>    
	<script src="../theme/assets/js/zabuto_calendar.js"></script>	
	
	
      <!-- js placed at the end of the document so the pages load faster -->
  
    <script src="../theme/assets/js/jquery.ui.touch-punch.min.js"></script>

    <script type="text/javascript" src="../theme/assets/js/validator.min.js"></script>
    
  </body>
</html>
