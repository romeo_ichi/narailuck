 <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="../theme/assets/img/user.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php //echo $_SESSION['ename'];?> <?php //echo $_SESSION['elname'];?></h5>
              	  	
                  <li class="sub-menu">
                      <a class="active" href="../home/">
                          <i class="fa fa-home"></i>
                          <span>หน้าหลัก</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="../home_text/">
                          <i class="fa fa-pencil-square-o"></i>
                          <span>ข้อความหน้าเว็บ</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a  href="../menu_web/">
                          <i class="fa fa-dashboard"></i>
                          <span>เมนู</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a  href="../slide_web/">
                          <i class="fa fa-picture-o"></i>
                          <span>สไลค์</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="../payment/" >
                          <i class="fa fa-credit-card"></i>
                          <span>ข้อความการแจ้งชำระเงิน</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="../add/" >
                          <i class="fa fa-university"></i>
                          <span>ที่อยู่</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="../lang/" >
                          <i class="fa fa-flag"></i>
                          <span>ภาษา</span>
                      </a>
                  </li>
                  

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>ข้อมูลหลัก</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../category/"><i class="fa fa-cog"></i>ประเภทสินค้า</a></li>
                          <li><a  href="../category_detail/"><i class="fa fa-cog"></i>ชนิดสินค้า</a></li>
                          <li><a  href="../product/"><i class="fa fa-cog"></i>สินค้า</a></li>
                      </ul>
                  </li>
                  
                  
                  <li class="sub-menu">
                      <a href="../banner/" >
                          <i class="fa fa-tablet"></i>
                          <span>โฆษณา</span>
                      </a>
                  </li>
                  
                  <li class="sub-menu">
                      <a href="../matter/" >
                          <i class="fa fa-flickr"></i>
                          <span>สร้างเพจ</span>
                      </a>
                  </li>
                  
                  
                  <li class="sub-menu">
                      <a href="../text_web/" >
                          <i class="fa fa-flickr"></i>
                          <span>รายละเอียดข้อความอื่นๆ</span>
                      </a>
                  </li>
                  <?php /*?><li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>รายงาน</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="../report/payment.php"><i class="fa fa-file-text"></i>การแจ้งชำระเงิน</a></li>
                          <li><a  href="../report/set.php"><i class="fa fa-file-text"></i>การจัดส่ง</a></li>
                          <li><a  href="../report/service.php"><i class="fa fa-file-text"></i>ข้อมูลพนักงาน</a></li>
                          <li><a  href="../report/member.php"><i class="fa fa-file-text"></i>ข้อมูลสมาชิก</a></li>
                          <li><a  href="../report/right.php"><i class="fa fa-file-text"></i>ข้อมูลสิทธิ์</a></li>
                          <li><a  href="../report/product.php"><i class="fa fa-file-text"></i>ข้อมูลสินค้า</a></li>
                          <li><a  href="../report/style.php"><i class="fa fa-file-text"></i>ข้อมูลรูปแบบสินค้า</a></li>
                          <li><a  href="../report/size.php"><i class="fa fa-file-text"></i>ข้อมูลขนาดสินค้า</a></li>
                          <li><a  href="../report/color.php"><i class="fa fa-file-text"></i>ข้อมูลสีสินค้า</a></li>
                          <li><a  href="../report/bank.php"><i class="fa fa-file-text"></i>ข้อมูลธนาคาร</a></li>
                      </ul>
                  </li><?php */?>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
<!--sidebar end-->

            