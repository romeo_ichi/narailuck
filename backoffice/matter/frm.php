<?php include("../theme/header/header.php");  ?>

<?php include("../theme/menu/menu_left.php");  ?>

<script src="../theme/ckeditor/ckeditor.js" type="text/javascript"></script>  
<script type="text/JavaScript" src="../theme/upload_pic/script.js"></script>
<link rel="stylesheet" type="text/css" href="../theme/upload_pic/style.css" />


<script language="JavaScript">
	function showPreview(ele)
	{
			$('#imgAvatar').attr('src', ele.value); // for IE
            if (ele.files && ele.files[0]) {
			
                var reader = new FileReader();
				
                reader.onload = function (e) {
                    $('#imgAvatar').attr('src', e.target.result);
                }

                reader.readAsDataURL(ele.files[0]);
            }
	}
</script> 
        
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>สร้างเพจ</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb">เพิ่มข้อมูล</h4>
                      <form class="form-horizontal style-form" id="signupForm" name="signupForm" method="post" enctype="multipart/form-data" action="">
                         
                          <div>
                          
                          <ul class="nav nav-tabs" role="tablist">
                             <?php
									
								$i=1; 
								$sql="select * from lang where lang_sta = 2";
								$result=mysql_query($sql);
								
								
								if($result){
									while($row = mysql_fetch_assoc($result)) {
							?>
                              <li role="presentation" class="<?php if($row['lang_id'] == '1'){ echo "active"; }?>"><a href="#meun<?php echo $row['lang_id'];?>" aria-controls="home" role="tab" data-toggle="tab"><?php echo $row['lang_name'];?></a></li>
                              
                                  
                          <?php
									$i++;}
								}
							?>
                            
                            </ul>
                            
                            <div class="tab-content">
                            <?php
                            	
								$z=1;
								$sql1="select * from lang where lang_sta = 2";
								$result1=mysql_query($sql1);
								if($result1){
									while($row1 = mysql_fetch_assoc($result1)) {
										
							
							?>
                              <div role="tabpanel" class="<?php if($row1['lang_id'] == '1'){ echo "tab-pane active"; }else{ echo "tab-pane"; }?>" id="meun<?php echo $row1['lang_id'];?>">
                                <div class="form_setion">
                                
                                
                                <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">ชื่อเพจภาษา<?php echo $row1['lang_name'];?></label>
                                      <div class="col-sm-10">
                                          <input type="text" name="mat_name<?php echo $row1['lang_id'];?>" id="mat_name<?php echo $row1['lang_id'];?>"  class="form-control" placeholder="ชื่อเพจ" />
                                      </div>
                                  </div>
                                  
                                  <div class="form-group">
                                      <label class="col-sm-2 col-sm-2 control-label">รายละเอียดภาษา<?php echo $row1['lang_name'];?></label>
                                      <div class="col-sm-10">
                                          
                                       <textarea name="mat_detail<?php echo $row1['lang_id'];?>" class="form-control" rows="8" style="width:400px;" id="full_description<?php echo $row1['lang_id'];?>" ></textarea>    
                                          
                                          
                                          <input type="hidden" name="lang_id<?php echo $row1['lang_id'];?>" id="lang_id<?php echo $row1['lang_id'];?>" value="<?php echo $row1['lang_id'];?>">
                                      </div>
                                  </div>

                                  

                                </div>

                              </div>
                              
                            <?php
                            
									$z++;}
								}
							?>  
                            </div>
                            
                            <div class="tab-content">
                              <div role="tabpanel" class="tab-pane active" id="home">
                                <div class="form_setion">
                                  
                                  <div class="form-group">
                                       <label class="col-sm-2 col-sm-2 control-label">รูป</label>
                                      <div class="col-sm-10">
                                          <input type="file" name="mat_pic" id="mat_pic" class="form-control" placeholder="รูป" OnChange="showPreview(this)">
                                          
                                            <br />
                                            <img id="imgAvatar" style="width:20%;">
                                      </div>
                                  </div>

                                </div>

                              </div>
                            </div>
                            <hr>
                          <button type="submit" class="btn btn-primary">บันทึก</button>
                          <button type="reset" class="btn btn-warning">ล้างข้อมูล</button>
                          <a class="btn btn-success" style="" href="index.php">กลับสู่หน้าข้อมูล</a> 
                          
                          
                          </div>
                          
                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
          	
          	
		</section><!--/wrapper -->
      </section>
      
      

      <!--main content end-->


<!-- Modal -->
     <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
      <div class="modal-dialog">
          <div class="modal-body">
            
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" onClick="javascript:window.location = 'index.php';">&times;</button>
                ดำเนินการเรียบร้อย [<a href="index.php" class="fw_bold" style="cursor:pointer">ตกลง</a>]
            </div>
            
          </div>
      </div>
    </div>



   <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
      <div class="modal-dialog">
          <div class="modal-body">
            
            <div class="alert alert-danger alert-dismissable">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
            		กรุณากรอบข้อความภาษาไทยด้วยค่ะ
            </div>
            
          </div>
      </div>
    </div>



<script type="text/javascript">

CKEDITOR.replace( 'full_description1', {
    removeDialogTabs : 'image:advanced;image:Link;link:advanced;link:upload',
    toolbar: [
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'document', items: [ 'NewPage', 'Preview', '-', 'Templates' ] }, 
            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],[ 'Bold', 'Italic' ],['JustifyLeft' , 'JustifyCenter' , 'JustifyRight' , 'JustifyBlock'],
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'insert', items : [ 'Image' ] },
            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }
        ],
        
        filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?Type=Images',
		filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?Type=Flash',
		filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
//timer = setInterval('updateDiv()',100);
function updateDiv(){
    var editorText = CKEDITOR.instances.full_description.getData();
    return editorText;
}


</script>



<script type="text/javascript">

CKEDITOR.replace( 'full_description2', {
    removeDialogTabs : 'image:advanced;image:Link;link:advanced;link:upload',
    toolbar: [
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'document', items: [ 'NewPage', 'Preview', '-', 'Templates' ] }, 
            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],[ 'Bold', 'Italic' ],['JustifyLeft' , 'JustifyCenter' , 'JustifyRight' , 'JustifyBlock'],
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'insert', items : [ 'Image' ] },
            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }
        ],
        
        filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?Type=Images',
		filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?Type=Flash',
		filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
//timer = setInterval('updateDiv()',100);
function updateDiv(){
    var editorText = CKEDITOR.instances.full_description.getData();
    return editorText;
}


</script>



<script type="text/javascript">

CKEDITOR.replace( 'full_description3', {
    removeDialogTabs : 'image:advanced;image:Link;link:advanced;link:upload',
    toolbar: [
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'document', items: [ 'NewPage', 'Preview', '-', 'Templates' ] }, 
            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],[ 'Bold', 'Italic' ],['JustifyLeft' , 'JustifyCenter' , 'JustifyRight' , 'JustifyBlock'],
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'insert', items : [ 'Image' ] },
            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }
        ],
        
        filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?Type=Images',
		filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?Type=Flash',
		filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
//timer = setInterval('updateDiv()',100);
function updateDiv(){
    var editorText = CKEDITOR.instances.full_description.getData();
    return editorText;
}


</script>


<script type="text/javascript">

CKEDITOR.replace( 'full_description4', {
    removeDialogTabs : 'image:advanced;image:Link;link:advanced;link:upload',
    toolbar: [
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'document', items: [ 'NewPage', 'Preview', '-', 'Templates' ] }, 
            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],[ 'Bold', 'Italic' ],['JustifyLeft' , 'JustifyCenter' , 'JustifyRight' , 'JustifyBlock'],
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'insert', items : [ 'Image' ] },
            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }
        ],
        
        filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?Type=Images',
		filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?Type=Flash',
		filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
//timer = setInterval('updateDiv()',100);
function updateDiv(){
    var editorText = CKEDITOR.instances.full_description.getData();
    return editorText;
}


</script>



<script type="text/javascript">

CKEDITOR.replace( 'full_description5', {
    removeDialogTabs : 'image:advanced;image:Link;link:advanced;link:upload',
    toolbar: [
	{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
            { name: 'document', items: [ 'NewPage', 'Preview', '-', 'Templates' ] }, 
            [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],[ 'Bold', 'Italic' ],['JustifyLeft' , 'JustifyCenter' , 'JustifyRight' , 'JustifyBlock'],
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
			{ name: 'insert', items : [ 'Image' ] },
            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }
        ],
        
        filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?Type=Images',
		filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?Type=Flash',
		filebrowserUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
    });
//timer = setInterval('updateDiv()',100);
function updateDiv(){
    var editorText = CKEDITOR.instances.full_description.getData();
    return editorText;
}


</script>


 <script type="text/javascript">

 $("form#signupForm").submit(function(){
	 
	for (instance in CKEDITOR.instances) {
	 CKEDITOR.instances[instance].updateElement();
	}
		

    var formData = new FormData($(this)[0]);

    $.ajax({
        url: "insert.php",
        type: 'POST',
        data: formData,
        async: false,
        success: function (data) {
            suss(data)
        },
        cache: false,
        contentType: false,
        processData: false
    });

    return false;
});	


function suss(msg)
		{
					if($.trim(msg) == "error")
					{
							
							$('#myModal2').modal({
							  backdrop:'static'
							});	
							setTimeout(function(){
								$('#myModal2').modal('hide');
							}, 3000);
							
					}else if($.trim(msg) == "suss"){
						
							$('#myModal1').modal({
								  backdrop:'static'
							});
						
					}else{
							
							alert(msg);
						
					}
		}	

</script> 

<?php include("../theme/footer/footer.php");  ?>


