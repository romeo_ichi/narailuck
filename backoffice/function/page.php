<style type="text/css">  
/* css แบ่งหน้า */  
.browse_page{     
    clear:both;     
    margin-left:12px;     
    height:25px;     
    margin-top:5px;     
    display:block;     
}     
.browse_page a,.browse_page a:hover{     
    display:block;     
    width: 2%;  
    font-size:14px;     
    float:left;     
    margin:0px 5px;  
    border:1px solid #CCCCCC;     
    background-color:#F4F4F4;     
    color:#333333;     
    text-align:center;     
    line-height:22px;     
    font-weight:bold;     
    text-decoration:none;     
    -webkit-border-radius: 5px;  
    -moz-border-radius: 5px;  
    border-radius: 5px;   
}     
.browse_page a:hover{     
    border:1px solid #CCCCCC;  
    background-color:#999999;  
    color:#FFFFFF;     
}     
.browse_page a.selectPage{     
    display:block;     
    width:45px;     
    font-size:14px;     
    float:left;     
    margin-right:2px;     
    border:1px solid #CCCCCC;  
    background-color:#999999;  
    color:#FFFFFF;     
    text-align:center;     
    line-height:22px;      
    font-weight:bold;     
    -webkit-border-radius: 5px;  
    -moz-border-radius: 5px;  
    border-radius: 5px;   
}     
.browse_page a.SpaceC{     
    display:block;     
    width:45px;     
    font-size:14px;     
    float:left;     
    margin-right:2px;     
    border:0px dotted #0A85CB;     
    background-color:#FFFFFF;     
    color:#333333;     
    text-align:center;     
    line-height:22px;     
    font-weight:bold;     
    -webkit-border-radius: 5px;  
    -moz-border-radius: 5px;  
    border-radius: 5px;   
}     
.browse_page a.naviPN{     
    width:50px;     
    font-size:12px;     
    display:block;     
/*    width:25px;   */  
    float:left;     
    border:1px solid #CCCCCC;  
    background-color:#999999;  
    color:#FFFFFF;     
    text-align:center;     
    line-height:22px;     
    font-weight:bold;        
    -webkit-border-radius: 5px;  
    -moz-border-radius: 5px;  
    border-radius: 5px;   
}    
/* จบ css แบ่งหน้า */  
</style>  




<?php  
// ฟังก์ชั่นสำหรับการแบ่งหน้า NEW MODIFY  
function page_navi($before_p,$plus_p,$total,$total_p,$chk_page){        
    global $urlquery_str;     
    $pPrev=$chk_page-1;     
    $pPrev=($pPrev>=0)?$pPrev:0;     
    $pNext=$chk_page+1;     
    $pNext=($pNext>=$total_p)?$total_p-1:$pNext;          
    $lt_page=$total_p-4;     
    if($chk_page>0){       
        echo "<a  href='$urlquery_str"."pages=".intval($pPrev+1)."' class='naviPN'>Prev</a>";     
    }     
    if($total_p>=11){     
        if($chk_page>=4){     
            echo "<a $nClass href='$urlquery_str"."pages=1'>1</a><a class='SpaceC'>. . .</a>";        
        }     
        if($chk_page<4){     
            for($i=0;$i<$total_p;$i++){       
                $nClass=($chk_page==$i)?"class='selectPage'":"";     
                if($i<=4){     
                echo "<a $nClass href='$urlquery_str"."pages=".intval($i+1)."'>".intval($i+1)."</a> ";        
                }     
                if($i==$total_p-1 ){      
                echo "<a class='SpaceC'>. . .</a><a $nClass href='$urlquery_str"."pages=".intval($i+1)."'>".intval($i+1)."</a> ";        
                }            
            }     
        }     
        if($chk_page>=4 && $chk_page<$lt_page){     
            $st_page=$chk_page-3;     
            for($i=1;$i<=5;$i++){     
                $nClass=($chk_page==($st_page+$i))?"class='selectPage'":"";     
                echo "<a $nClass href='$urlquery_str"."pages=".intval($st_page+$i+1)."'>".intval($st_page+$i+1)."</a> ";           
            }     
            for($i=0;$i<$total_p;$i++){       
                if($i==$total_p-1 ){      
                $nClass=($chk_page==$i)?"class='selectPage'":"";     
                echo "<a class='SpaceC'>. . .</a><a $nClass href='$urlquery_str"."pages=".intval($i+1)."'>".intval($i+1)."</a> ";        
                }            
            }                                        
        }        
        if($chk_page>=$lt_page){     
            for($i=0;$i<=4;$i++){     
                $nClass=($chk_page==($lt_page+$i-1))?"class='selectPage'":"";     
                echo "<a $nClass href='$urlquery_str"."pages=".intval($lt_page+$i)."'>".intval($lt_page+$i)."</a> ";        
            }     
        }             
    }else{     
        for($i=0;$i<$total_p;$i++){       
            $nClass=($chk_page==$i)?"class='selectPage'":"";     
            echo "<a href='$urlquery_str"."pages=".intval($i+1)."' $nClass  >".intval($i+1)."</a> ";        
        }            
    }        
    if($chk_page<$total_p-1){     
        echo "<a href='$urlquery_str"."pages=".intval($pNext+1)."'  class='naviPN'>Next</a>";     
    }     
}  
?>  