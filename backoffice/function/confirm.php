<!-- Modal -->
 
    <div class="modal fade" id="pop_confirmDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
      <div class="modal-dialog">
          <div class="modal-body">
            
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                ต้องการลบข้อมูล ? [ <a onClick="onDelete();" class="fw_bold" style="cursor:pointer">ตกลง</a> | <a onClick="_cancle('pop_confirmDelete')" style="cursor:pointer" class="fw_bold">ยกเลิก</a> ]
            </div>
            
          </div>
      </div>
    </div>
    
    
    
    <div class="modal fade" id="pop_success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
      <div class="modal-dialog">
          <div class="modal-body">
            
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                
                <span id="pop_success_label">ดำเนินการเรียบร้อย</span>
            </div>
            
          </div>
      </div>
    </div>
    
    
    <div class="modal fade" id="pop_error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
      <div class="modal-dialog">
          <div class="modal-body">
            
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                เกิดข้อผิดพลาดในระบบ
            </div>
            
          </div>
      </div>
    </div>






<script language="JavaScript">
	function _confirm(id)
	{
		$('#pop_confirmDelete').modal({
				  backdrop:'static'
			});
	}	
	
	function _cancle(id)
	{
		$('#pop_confirmDelete').modal('hide');
	}
		
</script>