<?php 

session_start();

if(isset($_SESSION["eper"])){
	
	if($_SESSION["eper"] != 1){
	
	$_SESSION["eper"] = '';
	
	}else{
		
		echo '<script type="text/javascript">window.location="../home/";</script>';
		
	}


}




?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>DASHGUM - Bootstrap Admin Template</title>

    <!-- Bootstrap core CSS -->
    <link href="../theme/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../theme/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        
    <!-- Custom styles for this template -->
    <link href="../theme/assets/css/style.css" rel="stylesheet">
    <link href="../theme/assets/css/style-responsive.css" rel="stylesheet">
    
    <link href="../theme/assets/plugins/validate.css" rel="stylesheet" />
    
    <script src="../theme/assets/plugins/jquery.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body onLoad="loading()">

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
	  	
		      <form class="form-login" id="signupForm" name="signupForm" method="post" action="">
              
		        <h2 class="form-login-heading">sign in now</h2>
		        <div class="login-wrap">
		            <input type="text" class="form-control" placeholder="User ID" id="user" name="user" autofocus>
		            <br>
		            <input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
		            <label class="checkbox">
		                <span class="pull-right">
		                    <a data-toggle="modal" href="login.html#myModal"> Forgot Password?</a>
		
		                </span>
		            </label>
		            <button class="btn btn-theme btn-block" type="submit"><i class="fa fa-lock"></i> SIGN IN</button>
                    <br/>
                    <a href="../../home/"><button class="btn btn-warning btn-block" type="button">กลับสู่หน้าเว็บไซค์</button></a>
		            <hr>
                    
                    
					 <div id="loading" style="margin:0 0 0 99px;"><img src="../theme/assets/img/page-loader.gif" style="height:100px; width:100px;"></div>
                     
                     
		        </div>
		 
		
		      </form>	  	
	  	
	  	</div>
	  </div>
      
      <!-- Modal -->
		          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
		              <div class="modal-dialog">
		                  <div class="modal-content">
		                      <div class="modal-header">
		                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                          <h4 class="modal-title">Forgot Password ?</h4>
		                      </div>
		                      <div class="modal-body">
		                          <p>Enter your e-mail address below to reset your password.</p>
		                          <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
		
		                      </div>
		                      <div class="modal-footer">
		                          <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
		                          <button class="btn btn-theme" type="button">Submit</button>
		                      </div>
		                  </div>
		              </div>
		          </div>
                  
                  
                  
                 <div class="modal fade" id="pop_success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
                  <div class="modal-dialog">
                      <div class="modal-body">
                        
                        <div class="alert alert-success alert-dismissable">
                            <a href="index.php"><button type="button" class="close">&times;</button></a>
                            ยินดีต้อนรับ เข้าสู่ระบบ 
                        </div>
                        
                      </div>
                  </div>
                </div>
                
                
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="pop_error" class="modal fade" style="margin-top:100px;">
                  <div class="modal-dialog">
                      <div class="modal-body">
                        
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            เกิดข้อผิดพลาด เนื่องจาก ชื่อผู้ใช้าน หรือ รหัสผ่าน ไม่่ถูกต้อง กรุณาลองใหม่
                        </div>
                        
                      </div>
                  </div>
                </div>
	<!-- modal -->
      
         
      
    <script type="text/javascript">
	
	function loading(){
		
		$('#loading').fadeOut();
		
	}
	

	$().ready(function() {
		
		/*setTimeout(function(){
				$('#loading').html('<img src="../theme/assets/img/page-loader.gif">');
		}, 1000);*/
		
		
		$("#signupForm").validate({
			rules: {
				user: {
					required: true,
					minlength: 4, 
				},
				pass: {
					required: true,
					minlength: 4,
				}
			},
			messages: {
					
		
			},/*
			success:function(label){
								label.html(" <div class='hover_alert'><img src='../theme/assets/img/page-loader.gif'></div>");
			},*/
			submitHandler: function(form) {
				
									$('#loading').fadeIn();
									
										
										$(form).ajaxSubmit({url: 'login_session.php', type: 'post', success:function(msg){suss(msg)} });
									
			} 
		});
	
	
	});
	
	
	function suss(msg)
			{
						if($.trim(msg) == "error")
						{
								$('#loading').fadeOut();
								
								$('#pop_error').modal({
									  backdrop:'static'
								});
								
								setTimeout(function(){
									$('#pop_error').modal('hide');
								}, 4000);
								
						}else if($.trim(msg) == "suss"){
							
							
								$('#loading').fadeOut();
							
								setTimeout(function(){
								$('#pop_success').modal({
									  backdrop:'static'
								});
								}, 1000);
								
								setTimeout(function(){
									$('#pop_success').modal('hide');
								}, 2000);
								
								setTimeout(function(){
								 	window.location='../home/index.php';
								}, 3000);
							
						}else{
								
								alert(msg);
							
						}
			}	

	</script>   

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../theme/assets/js/jquery.js"></script>
    <script src="../theme/assets/js/bootstrap.min.js"></script>
	<script src="../theme/assets/plugins/jquery.validate.th.js"></script>
    <script src="../theme/assets/plugins/jquery.form.min.js"></script>
    <!--BACKSTRETCH-->
    <!-- You can use an image of whatever size. This script will stretch to fit in any screen size.-->
    <script type="text/javascript" src="../theme/assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("../theme/assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>