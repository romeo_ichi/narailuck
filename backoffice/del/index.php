<?php 

	session_start(); 
	
	
	if($_SESSION["per"] != 1){
	
	echo '<script type="text/javascript">window.location="../login";</script>';
	$_SESSION["per"] = '';
	
	}else{
		
		$_SESSION["per"] = 1;	
		
	}
	include("../../Database/Database.php");
	
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>TK Shop</title>

    <!-- Bootstrap core CSS -->
    <link href="../theme/assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../theme/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../theme/assets/css/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="../theme/assets/js/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="../theme/assets/lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="../theme/assets/css/style.css" rel="stylesheet">
    <link href="../theme/assets/css/style-responsive.css" rel="stylesheet">

    <script src="../theme/assets/js/chart-master/Chart.js"></script>
    
    <!--<script src="../theme/assets/plugins/jquery.js"></script> 
    
    <script src="../theme/assets/plugins/jquery.form.min.js"></script>
    
     HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b>TK Shop</b></a>
            <!--logo end-->
            <?php /*?><div class="nav notify-row" id="top_menu">
                <!--  notification start -->
                <ul class="nav top-menu">
                    <!-- settings start -->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                            <i class="fa fa-tasks"></i>
                            <span class="badge bg-theme">4</span>
                        </a>
                        <ul class="dropdown-menu extended tasks-bar">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
                                <p class="green">You have 4 pending tasks</p>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <div class="task-info">
                                        <div class="desc">DashGum Admin Panel</div>
                                        <div class="percent">40%</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <div class="task-info">
                                        <div class="desc">Database Update</div>
                                        <div class="percent">60%</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <div class="task-info">
                                        <div class="desc">Product Development</div>
                                        <div class="percent">80%</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <div class="task-info">
                                        <div class="desc">Payments Sent</div>
                                        <div class="percent">70%</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                            <span class="sr-only">70% Complete (Important)</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="external">
                                <a href="#">See All Tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                    <li id="header_inbox_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                            <i class="fa fa-envelope-o"></i>
                            <span class="badge bg-theme">5</span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
                                <p class="green">You have 5 new messages</p>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <span class="photo"><img alt="avatar" src="../theme/assets/img/ui-zac.jpg"></span>
                                    <span class="subject">
                                    <span class="from">Zac Snider</span>
                                    <span class="time">Just now</span>
                                    </span>
                                    <span class="message">
                                        Hi mate, how is everything?
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <span class="photo"><img alt="avatar" src="../theme/assets/img/ui-divya.jpg"></span>
                                    <span class="subject">
                                    <span class="from">Divya Manian</span>
                                    <span class="time">40 mins.</span>
                                    </span>
                                    <span class="message">
                                     Hi, I need your help with this.
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <span class="photo"><img alt="avatar" src="../theme/assets/img/ui-danro.jpg"></span>
                                    <span class="subject">
                                    <span class="from">Dan Rogers</span>
                                    <span class="time">2 hrs.</span>
                                    </span>
                                    <span class="message">
                                        Love your new Dashboard.
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">
                                    <span class="photo"><img alt="avatar" src="../theme/assets/img/ui-sherman.jpg"></span>
                                    <span class="subject">
                                    <span class="from">Dj Sherman</span>
                                    <span class="time">4 hrs.</span>
                                    </span>
                                    <span class="message">
                                        Please, answer asap.
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="index.html#">See all messages</a>
                            </li>
                        </ul>
                    </li>
                    <!-- inbox dropdown end -->
                </ul>
                <!--  notification end -->
            </div><?php */?>
            <form id="frmlogout" name="frmlogout" action="" method="post">
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li>
                        <a onclick="logout();" class="logout" style="cursor:pointer;"><i class="fa fa-sign-out fa-fw"></i>Logout</a>
                    </li>
            	</ul>
            </div>
            </form>
        </header>
      <!--header end-->


<div class="modal fade" id="pop_logout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
      <div class="modal-dialog">
          <div class="modal-body">
            
            <div class="alert alert-success alert-dismissable">
                <a href="logout.php"><button type="button" class="close">&times;</button></a>
                ออกจากระบบ เรียบร้อย
            </div>
            
          </div>
      </div>
</div>

   

 <script type="text/javascript">
	
	function logout(){
		
		/*
		alert('testset');*/
		$.ajax({
			type: "POST",
			url: '../login/logout.php',
			data: $("#frmlogout").serialize(), // serializes the form's elements.
			success: function (msg) {
				respond(msg)
	
			}
		});
		
	}
	
	function respond(msg) {
		if ($.trim(msg) == "suss") {
			
			$('#pop_logout').modal({
				  backdrop:'static'
			});
			
			setTimeout(function(){
				$('#pop_logout').modal('hide');
			}, 1000);
			
			setTimeout(function(){
				window.location='../login/';
			}, 2000);
		}
		else {
			
			alert(msg);
		}
	}
	
	
	</script>
    

<!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <p class="centered"><a href="profile.html"><img src="../theme/assets/img/user.jpg" class="img-circle" width="60"></a></p>
              	  <h5 class="centered"><?php echo $_SESSION['name'];?> <?php echo $_SESSION['lname'];?></h5>
              	  	
                  <li class="mt">
                      <a class="active" href="index.html">
                          <i class="fa fa-dashboard"></i>
                          <span>หน้าหลัก</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-credit-card"></i>
                          <span>การแจ้งชำระเงิน</span>
                      </a>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-cogs"></i>
                          <span>ข้อมูลหลัก</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="calendar.html"><i class="fa fa-cog"></i>Calendar</a></li>
                          <li><a  href="gallery.html"><i class="fa fa-cog"></i>Gallery</a></li>
                          <li><a  href="todo_list.html"><i class="fa fa-cog"></i>Todo List</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="fa fa-book"></i>
                          <span>รายงาน</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="blank.html"><i class="fa fa-file-text"></i>Blank Page</a></li>
                          <li><a  href="login.html"><i class="fa fa-file-text"></i>Login</a></li>
                          <li><a  href="lock_screen.html"><i class="fa fa-file-text"></i>Lock Screen</a></li>
                      </ul>
                  </li>

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
<!--sidebar end-->
        

<script language="JavaScript">
	function ClickCheckAll(vol)
	{
		var i=1;
		
		var hdnCount = $('#hdnCount').val();
		 
		for(i=1;i<=hdnCount;i++)
		{
			if(vol.checked == true)
			{
				eval("chkDel"+i+".checked=true");
				$('#btn_del').prop('disabled',false);
			}
			else
			{
				eval("chkDel"+i+".checked=false");
				$('#btn_del').prop('disabled',true);
			}
		}
			
	}
	
	
	function countCheckboxes(form)
	{ 
	
	  var numberOfChecked = $('input:checkbox:checked').length;
	  if(numberOfChecked >= 1 && $('#CheckAll').prop('checked',true))
	  { 
		  $('#CheckAll').prop('checked',false);  
		  $('#btn_del').prop('disabled',true);
	  }
	  if(numberOfChecked > 0 && $('#CheckAll').prop('checked',false))
	  {
		  $('#btn_del').prop('disabled',false);;
	  }
	  else
	  {
		  $('#btn_del').prop('disabled',true);; 
	  }
	}
		
</script>
        
         <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>ข้อมูลพนักงาน</h3>
				

              <div class="row mt">
                
                  <div class="col-md-12">
                      <div class="content-panel">
                      
                <form name="frmcon" id="frmcon" method="post" action="">      
                <div id="employer_load">
                
                          <table class="table table-striped table-advance table-hover">
	                  	  	  
	                  	  	  <hr>
                              <thead>
                              <tr>
                              	  <th width="5%"><input name="CheckAll" type="checkbox" id="CheckAll" value="Y" onClick="ClickCheckAll(this);"></th>
                                  <th>#รหัส</th>
		                          <th>ชื่อพนักงาน</th>
		                          <th>นามสกุล</th>
                                  <th>สิทธิ์การใช้งาน</th>
                                  <th><i class=" fa fa-edit"></i> Status</th>
                              </tr>
                              </thead>
                              
                              
                              <tbody>
                              <?php  
									$i=1;  
									$q="SELECT * FROM  employer em LEFT JOIN right_emp ri ON em.rig_id = ri.rig_id ";  
									
									$qr=@mysql_query($q);     
									           
									  
									while($rs=@mysql_fetch_array($qr)){  
							?>  
                              
                             <tr>
                              	  <td><input type="checkbox" onClick="countCheckboxes(form.id);" name="chkDel[]" id="chkDel<?php echo $i;?>" value="<?php echo $rs['emp_id'];?>"></td>	
                                  <td><?php echo $rs['emp_id'];?></td>
                                  <td><?php echo $rs['emp_name'];?></td>
                                  <td><?php echo $rs['emp_lname'];?></td>
                                  <td><?php echo $rs['rig_name'];?></td>
                                  <td>
                                      <a href="#"><button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button></a>
                                      <a href="edit.php?id=<?php echo $rs['emp_id']?>"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a>
                                      <a href="del.php?id=<?php echo $rs['emp_id']?>"><button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button></a>
                                  </td>
                             </tr> 
                              
                              
                              
                              
                              
                            <?php $i++; } ?>  
                            
                            
                              
                              <!--<tr>
                              	  <td><input type="checkbox" onClick="countCheckboxes(form.id);" name="chkDel[]" id="chkDel" value=""></td>	
                                  <td>001</td>
                                  <td>มาดี</td>
                                  <td>สบายดี </td>
                                  <td>ผู้ดูแลระบบ </td>
                                  <td>
                                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                  </td>
                              </tr>
                              <tr>
                              	  <td><input type="checkbox" onClick="countCheckboxes(form.id);" name="chkDel[]" id="chkDel" value=""></td>	
                                  <td>002</td>
                                  <td>สมชาย</td>
                                  <td>สมศรี</td>
                                  <td>พนักงาน</td>
                                  <td>
                                      <button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>
                                      <button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button>
                                      <button class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></button>
                                  </td>
                              </tr>-->
                              </tbody>
                              <input type="hidden" name="hdnCount" id="hdnCount" value="<?php echo $i;?>">
                                
                            
                              <tfoot>  
                              <tr>  
                                <td align="left" colspan="5">  
                              
                                     
                                </td>  
                                <td><button id="btn_del" type="button" class="pull-left btn btn-danger" disabled onclick="_confirm('pop_confirmDelete');"><i class="fa fa-trash-o "></i> ลบ</button></td>
                              </tr>  
                            </tfoot> 
                              
                          </table>
                       </div>   
                        </form>  
                      </div><!-- /content-panel -->
                  </div><!-- /col-md-12 -->
                  
              </div><!-- /row -->

		</section><! --/wrapper -->
      </section>
      

      <!--main content end-->
<?php include("../function/confirm.php");  ?>



<script type="text/javascript">

	function onDelete()
		{
			$.ajaxSetup({cache:false});
			$('#pop_confirmDelete').modal('hide');
			
			var formData = $('#frmcon').serialize();
			$.post("delete.php",(formData), function( data ) {
				
				if(data.result == true)
				{
					$('#pop_success').modal({
					  backdrop:'static'
					});	
					setTimeout(function(){
						$('#pop_success').modal('hide');
					}, 3000);
					$('#employer_load').load('load.php');
				}
				else
				{
					$('#pop_error').slideDown();
					setTimeout(function(){
						$('#pop_error').slideUp();
					}, 3000);
					return false;
				}	
			},'JSON');
	
		}
	
</script>



 </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../theme/assets/js/jquery.js"></script>
    <script src="../theme/assets/js/jquery-1.8.3.min.js"></script>
    <script src="../theme/assets/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../theme/assets/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../theme/assets/js/jquery.scrollTo.min.js"></script>
    <script src="../theme/assets/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../theme/assets/js/jquery.sparkline.js"></script>


    <!--common script for all pages-->
    <script src="../theme/assets/js/common-scripts.js"></script>
    
    <script type="text/javascript" src="../theme/assets/js/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="../theme/assets/js/gritter-conf.js"></script>

    <!--script for this page-->
    <script src="../theme/assets/js/sparkline-chart.js"></script>    
	<script src="../theme/assets/js/zabuto_calendar.js"></script>	
	
	
      <!-- js placed at the end of the document so the pages load faster -->
  
    <script src="../theme/assets/js/jquery.ui.touch-punch.min.js"></script>


    
  </body>
</html>



