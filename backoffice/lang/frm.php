<?php include("../theme/header/header.php");  ?>

<?php include("../theme/menu/menu_left.php");  ?>
 
<script type="text/JavaScript" src="../theme/upload_pic/script.js"></script>
<link rel="stylesheet" type="text/css" href="../theme/upload_pic/style.css" />


<script language="JavaScript">
	function showPreview(ele)
	{
			$('#imgAvatar').attr('src', ele.value); // for IE
            if (ele.files && ele.files[0]) {
			
                var reader = new FileReader();
				
                reader.onload = function (e) {
                    $('#imgAvatar').attr('src', e.target.result);
                }

                reader.readAsDataURL(ele.files[0]);
            }
	}
</script> 
        
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">
          	<h3>ภาษา</h3>
          	
          	<!-- BASIC FORM ELELEMNTS -->
          	<div class="row mt">
          		<div class="col-lg-12">
                  <div class="form-panel">
                  	  <h4 class="mb">เพิ่มภาษา</h4>
                      <form class="form-horizontal style-form" id="signupForm" name="signupForm" method="post" enctype="multipart/form-data" action="">
                          <div class="form-group">
                               <label class="col-sm-2 col-sm-2 control-label">ชื่อภาษา</label>
                              <div class="col-sm-10">
                                  <input type="text" name="pro_name" id="pro_name" class="form-control" placeholder="ชื่อภาษา">
                              </div>
                          </div>
                          <div class="form-group">
                               <label class="col-sm-2 col-sm-2 control-label">รูป</label>
                              <div class="col-sm-10">
                                  <input type="file" name="pro_pic" id="pro_pic" class="form-control" placeholder="รูป" OnChange="showPreview(this)">
                                  
                                    <br />
                                    <img id="imgAvatar" style="width:20%;">
                              </div>
                          </div>
                          
                          <button type="submit" class="btn btn-primary">บันทึก</button>
                          <button type="reset" class="btn btn-warning">ล้างข้อมูล</button>
                          <a class="btn btn-success" style="" href="index.php">กลับสู่หน้าข้อมูล</a> 
                          

                      </form>
                  </div>
          		</div><!-- col-lg-12-->      	
          	</div><!-- /row -->
          	
          	
		</section><!--/wrapper -->
      </section>
      
      

      <!--main content end-->


<!-- Modal -->
   <!-- <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <a href="index.php"><button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button></a>
            <h4 class="modal-title" id="myModalLabel">แจ้งเตือน</h4>
          </div>
          <div class="modal-body">
            <div id="loadtest">
            
              <center><h3>ดำเนินการเรียบร้อย</h3></center>
                
            </div>
          </div>
          <div class="modal-footer">
            <a href="index.php"><button type="button" class="btn btn-default">Close</button></a>
          </div>
        </div>
      </div>
    </div>-->
    
    
     <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top:100px;">
      <div class="modal-dialog">
          <div class="modal-body">
            
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" onClick="javascript:window.location = 'index.php';">&times;</button>
                ดำเนินการเรียบร้อย [<a href="index.php" class="fw_bold" style="cursor:pointer">ตกลง</a>]
            </div>
            
          </div>
      </div>
    </div>




 <script type="text/javascript">

$().ready(function() {
	
	$("#signupForm").validate({
		rules: {
			pro_name: {
				required: true,
			},
			pro_pic: {
				required: true,
			},
			
		},
		messages: {
				
	
		},
		submitHandler: function(form) {
								
								$(form).ajaxSubmit({url: 'insert.php', type: 'post', success:function(msg){suss(msg)} });
								

					   } 
	});


});


function suss(msg)
		{
					if($.trim(msg) == "error")
					{
							alert('test_error');
							
					}else if($.trim(msg) == "suss"){
						
							$('#myModal1').modal({
								  backdrop:'static'
							});
						
					}else{
							
							alert(msg);
						
					}
		}	

</script> 

<?php include("../theme/footer/footer.php");  ?>


