<ul id="nav_inner">
    <li><a href="/">หน้าหลัก</a></li>
    <li><a href="/amulets/">พระ-ของขลัง</a></li>
    <li><a href="/antiques/">ของเก่า</a></li>
    <li><a href="/land/">โรงแรม-ที่ดิน</a></li>
    <li><a href="/category/">หมวดหมู่</a></li>
    <li><a href="/knowledge/">สาระน่ารู้</a></li>
    <li><a href="/contact/">ติดต่อเรา</a></li>
</ul>