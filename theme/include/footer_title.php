
<section class="banner">
	<div class="container">
        <div class="banner728">
            <a href="#" target="_blank"><img src="../theme/assets/images/banner728-3.jpg" class="img-responsive" /></a>
        </div>

    </div>
</section>

<footer>
    
    <div id="footer" class="container">
        <div id="footer_inner">
            <div class="footerT"><a href="#" title="พระบูชา">พระบูชา</a><a href="#" title="พระเบญจภาคี">พระเบญจภาคี</a><a href="#" title="เครื่องราง">เครื่องราง</a> <a href="#" title="พระปิดตา">พระปิดตา</a> <a href="#" title="พระกรุ">พระกรุ</a> <a href="#" title="พระเนื้อว่าน">พระเนื้อว่าน</a> <a href="#" title="พระกริ่ง">พระกริ่ง</a> <a href="#" title="พระชัย">พระชัย</a> <a href="#" title="พระเนื้อชิน">พระเนื้อชิน</a> <a href="#" title="พระงาแกะ">พระงาแกะ</a> <a href="#" title="พระไม้แกะ">พระไม้แกะ</a>
<a href="#" title="พระรูปหล่อ">พระรูปหล่อ</a> <a href="#" title="พระเหรียญ">พระเหรียญ</a> <a href="#" title="พระเหรียญปั้ม">พระเหรียญปั้ม</a> <a href="#" title="พระเนื้อผง">พระเนื้อผง</a> <a href="#" title="พระเนื้อดิน">พระเนื้อดิน</a> <a href="#" title="ผ้ายันต์">ผ้ายันต์</a> <a href="#" title="ภาพเก่าเกจิอาจารย์ดัง">ภาพเก่าเกจิอาจารย์ดัง</a> <a href="#" title="ผงคลุกรัก">ผงคลุกรัก</a> <a href="#" title="เฟอร์นิเจอร์เก่า">เฟอร์นิเจอร์เก่า</a> <a href="#" title="หนังสือพระเครื่องเก่า">หนังสือพระเครื่องเก่า</a> </div>
            <div class="footerB">
                <div class="footerBL">
                    รับประกันพระแท้ ยินดีคืนเงินสดโดยไม่มีเงื่อนไขใดๆ ทั้งสิ้น
ดำเนินการโดย คุณธนภูมิ (ศานิต) สุวัณณะศรี
                </div>
                <div class="footerBC">
                    <div class="location">ศูนย์พระเครื่องมรดกไทย<br />พันธ์ทิพย์พลาซ่า ชั้น 7 งามวงศ์วาน</div>
                    <div class="mobile">โทร <br /><a href="tel:+66972407664">097-240-7664</a></div>
                    <div class="mail">Email <br /><a href="mailto:n-arai@hotmail.com">n-arai@hotmail.com</a></div>
                </div>
                <div class="footerBR hidden-sm hidden-xs">
                    ผู้เข้าชมทั้งหมด
                    <b>2158213 คน</b>
                    ผู้เข้าชมวันนี้
                    <b>1469 คน</b>
                    ออนไลน์ขณะนี้
                    <b>37 คน</b>
                </div>
                
            </div>
            <div class="footerRight">
                Copyright @ 2014 NARAILUCK.COM All Right Reserved including text, graphics, interfaces and design thereof are all rights reserved.
            </div>
        </div>
        
    </div>


</footer>