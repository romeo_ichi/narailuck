<?php include('../theme/include/header.php') ?>

	<div class="main">
        <div class="container">
            <?php include('../theme/include/slider.php') ?>
            
            <?php include('../theme/include/welcome.php') ?>
            
            
            <section class="home-category">
                <div class="cat_sec">
                    <h2>พระใหม่ ของขลัง เครื่องราง ผ้ายันต์ บาตรน้ำมนต์ ภาพเก่า</h2>
                    <div class="pro_sec">
                        <div class="row">
                            <?php for($i=0;$i<36;$i++)  { ?>
                                <div class="col-md-3 col-sm-4">
                                    <div class="product_box">
                                        <div class="propic">
                                            <a href="detail.php" target="_blank"><img src="../theme/assets/images/propic.jpg" class="img-responsive" /></a>
                                            <span class="crop"></span>
                                        </div>
                                        <div class="prodetail">
                                            <a href="detail.php"  target="_blank" title="กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2">กริ่ง หลวงพ่อโสธร ปี 2508 เลี่ยมนาดหลวงพ่อโสธร ปี 2หลวงพ่อโสธร ปี 2</a>
                                            <span>สถานะ : เช่าไปแล้ว | ผู้ชม 530</span>
                                        </div>
                                        <div class="product_bottom"></div>
                                    </div>
                                </div>
                                
                            <?php } ?>
                        </div>
                        <div class="propage">
                            <a href="#"><</a> <a href="#">Previous</a> | <a href="#">1</a> | <a href="#">2</a> | <a href="#">3</a> | <a href="#">4</a> | <a href="#">5</a> | <a href="#">Next</a> <a href="#">></a>
                        </div>
                    </div>

                </div>
            </section>

            
        </div>
    </div>

<?php include('../theme/include/footer.php') ?>
	