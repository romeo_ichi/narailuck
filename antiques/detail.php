<?php include('../theme/include/header.php') ?>

	<div class="main">
        <div class="container">
            <?php include('../theme/include/slider.php') ?>
            
            <?php include('../theme/include/welcome.php') ?>
            
            <section class="home-category">
                <div class="cat_sec">
                    <h2>พระบูชา ไม้แก่นจันทร์ หลวงปู่บุญ วัดกลางบางแก้ว </h2>
                    <div class="pro_sec">
                        <div class="col-md-8">
                            <div class="proD_img">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <img src="../theme/assets/images/detail_fpic.jpg" class="img-responsive" />
                                        <figcaption>ด้านหน้า</figcaption>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="../theme/assets/images/detail_fpic.jpg" class="img-responsive" />
                                        <figcaption>ด้านหลัง</figcaption>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <img src="../theme/assets/images/detail_fpic.jpg" class="img-responsive" />
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="../theme/assets/images/detail_fpic.jpg" class="img-responsive" />
                                    </div>
                                    <div class="col-sm-4">
                                        <img src="../theme/assets/images/detail_fpic.jpg" class="img-responsive" />
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="proD_contents">
                                <dl>
                                  <dt>รุ่นพระ</dt>
                                  <dd>พระบูชา ไม้แก่นจันทร์ หลวงปู่บุญ วัดกลางบางแก้ว</dd>
                                  <dt>ประเภทพระ</dt>
                                  <dd>พระบูชา</dd>
                                  <dt>รายละเอียด</dt>
                                  <dd>
                                 สิงห์หลวงพ่อหอมตัวใหญ่สวยเดิมน่าสะสมบูชาตัวใหญ่สวยเดิม
                                  </dd>
                                  <dt>ราคา</dt>
                                  <dd>
                                        โทรสอบถาม
                                  </dd>
                                  <dt>ติดต่อ</dt>
                                  <dd>
                                        ร้าน นารายณ์รักษ์ พันธ์ทิพย์ ชั้น 7 งามวงศ์วาน  เปิดร้านตั้งแต่ เวลา 10.30น - 20.00น<br />
                                        <i aria-hidden="true" class="fa fa-phone"></i>โทร <a href="tel:+66972407664">097-240-7664</a> | <i aria-hidden="true" class="fa fa-envelope"></i> อีเมล <a href="mailto:n-arai@hotmail.com">n-arai@hotmail.com</a>
                                  </dd>
                                  <dt>ช่องทางการชำระเงิน</dt>
                                  <dd>
                                        ธนาคารไทยพาณิชย์ <br />สาขาพันทิพย์ พลาซ่า งามวงศ์วาน<br />
                                        เลขที่บัญชี <strong>388-211639-2</strong><br />
ชื่อบัญชี <strong>นายเกษม สุวัณณะศรี</strong>
                                  </dd>
                                </dl>
                                
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </section>

            
        </div>
    </div>

<?php include('../theme/include/footer.php') ?>
	